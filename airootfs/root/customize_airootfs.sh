#!/bin/bash

set -e -u

locale-gen

ln -sf /usr/share/zoneinfo/Europe/Madrid /etc/localtime

usermod -s /usr/bin/zsh root
cp -aT /etc/skel/ /root/
chmod 700 /root

if [[ -d "/root/customPkgs" ]]; then
    sh -c "cd /root/customPkgs&&ls *.tar.xz|pacman -U - --noconfirm"
    rm -rf "/root/customPkgs"
fi

# sed -i 's/#\(PermitRootLogin \).\+/\1yes/' /etc/ssh/sshd_config
sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf

sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf
echo '%wheel ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers

systemctl enable pacman-init.service choose-mirror.service
systemctl set-default multi-user.target

pacman-key --init && pacman-key --recv-keys DDF7DB817396A49B2A2723F7403BD972F75D9D76

chown -R 0:0 /root
id live || useradd -s /bin/zsh -G wheel -m live
echo -e "live\nlive"|passwd live
sudo -u live sh -c 'cd /pkgs&&for n in $(ls); do cd $n; makepkg -si --noconfirm; cd ..; done'
pacman -Qdtq && pacman -Rusn $(pacman -Qdtq)
rm -rf /pkgs
